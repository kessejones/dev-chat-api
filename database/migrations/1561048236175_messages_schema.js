'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MessagesSchema extends Schema {
    up() {
        this.create('messages', (table) => {
            table.increments('id_message')
            table.text('text').notNullable()
            table.integer('id_user_sender').unsigned().references('id_user').inTable('users')
            table.integer('id_user_recipient').unsigned().references('id_user').inTable('users')
            table.timestamps()
        })
    }

    down() {
        this.drop('messages')
    }
}

module.exports = MessagesSchema
