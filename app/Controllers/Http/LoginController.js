'use strict'

const { validate } = use('Validator');

class LoginController {
    async login({ request, auth }){
        const validation = await validate(request.all, {
            email: 'required',
            password: 'required'
        })

        if(validation.fails())
            return validation.messages()

        return await auth.attempt(validation.email, validation.password)
    }

    async logout({ auth }){
        await auth.logout()
    }
}

module.exports = LoginController
