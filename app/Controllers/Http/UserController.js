'use strict'

const { validate } = use('Validator');
const User = use('App/Models/User')

class UserController {
    async users(){
        const users = await User.all()

        return users
    }

    user(user) {
        return user
    }

    async store({ request }) {
        const validation = await validate(request.all(), {
            name: 'required',
            email: 'required|unique:users,id_users',
            password: 'required'
        })
        
        if(validation.fails())
            return validation.messages()

        const user = new User();
        user.fill(validation.data());
        user.save()

        return user
    }
}

module.exports = UserController
