'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Message extends Model {
    static get table(){
        return 'messages'
    }

    static get primaryKey(){
        return 'id_message'
    }

    static get visible(){
        return ['text']
    }

    user_sender(){
        return this.hasOne('App/Models/User', 'id_user', 'id_user_sender')
    }

    user_repient(){
        return this.hasOne('App/Models/User', 'id_user', 'id_user_recepient')
    }
}

module.exports = Message
